
FROM openjdk:11
EXPOSE 2222
MAINTAINER idris.com

ENV PROFILE=development

COPY src/main/resources/application.properties application.properties
COPY src/main/resources/port.properties port.properties
COPY target/springboot-docker-0.0.1-SNAPSHOT.jar springboot-docker-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/springboot-docker-0.0.1-SNAPSHOT.jar","--spring.profiles.active=${PROFILE}","--spring.config.location=file:/application.properties,file:/port.properties"]
